parser grammar EventsFileParser;

options {
	tokenVocab = EventsFileLexer;
}

file: expression*;

expression
    : lhs=T_LITERAL T_EQUALS_SIGN scope #ScopeExpression
    | lhs=T_LITERAL T_EQUALS_SIGN rhs=T_LITERAL #SimpleExpression
    ;

scope: T_LEFT_CURLY_BRACKET expression* T_RIGHT_CURLY_BRACKET;
