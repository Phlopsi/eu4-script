lexer grammar EventsFileLexer;

//T_COLON: ':';

T_EQUALS_SIGN: '=';

//T_COMMERCIAL_AT: '@';

T_LEFT_CURLY_BRACKET: '{';

T_RIGHT_CURLY_BRACKET: '}';

T_LITERAL
    : STRING
    | DATE
    | DECIMAL
    | INTEGER
    | (IDENTIFIER '@')? IDENTIFIER (':' IDENTIFIER)?
    ;

fragment
STRING: '"' ~('\r' | '\n' | '"')* '"';

fragment
ONE_TO_TWENTY_EIGHT
    :      '1'..'9'
    | '1'  '0'..'9'
    | '2'  '0'..'8'
    ;

fragment
ONE_TO_THIRTY
    :           '1'..'9'
    | '1'..'2'  '0'..'9'
    |      '3'       '0'
    ;

fragment
ONE_TO_THIRTY_ONE
    :           '1'..'9'
    | '1'..'2'  '0'..'9'
    |      '3'  '0'..'1'
    ;

fragment
TWO_TO_NINE_THOUSAND_NINE_HUNDRED_NINTY_NINE
    :                               '2'..'9'
    |                     '1'..'9'  '0'..'9'
    |           '1'..'9'  '0'..'9'  '0'..'9'
    | '1'..'9'  '0'..'9'  '0'..'9'  '0'..'9'
    ;

fragment
TWENTY_NINE_DAYS_MONTHS: '2';

fragment
THIRTY_DAYS_MONTHS
    :      '4'
    |      '6'
    |      '9'
    | '1'  '1'
    ;

fragment
THIRTY_ONE_DAYS_MONTHS
    :      '1'
    |      '3'
    |      '5'
    |      '7'
    |      '8'
    | '1'  '0'
    | '1'  '2'
    ;

fragment
DATE
    : '0'* TWO_TO_NINE_THOUSAND_NINE_HUNDRED_NINTY_NINE '.' (
              '0'* THIRTY_ONE_DAYS_MONTHS  '.' '0'* ONE_TO_THIRTY_ONE
            | '0'* TWENTY_NINE_DAYS_MONTHS '.' '0'* ONE_TO_TWENTY_EIGHT
            | '0'* THIRTY_DAYS_MONTHS      '.' '0'* ONE_TO_THIRTY
        )
    ;

fragment
DECIMAL
    : '-' [0-9]+ '.' [0-9]+
    |     [0-9]+ '.' [0-9]+
    ;

fragment
INTEGER
    : '-' [0-9]+
    |     [0-9]+
    ;

fragment
IDENTIFIER: [a-zA-Z0-9_.]+;

T_LINE_COMMENT: '#' ~('\n'|'\r')* -> skip;
T_WHITESPACE: [\n\r\t ]+ -> skip;
